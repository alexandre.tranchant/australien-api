##Australian shepherd API

Php 7.2 -
Symfony 4.1 -
Api-Platform 2.1

[![Build Status](https://travis-ci.org/Alexandre-T/australien-api.svg?branch=master)](https://travis-ci.org/Alexandre-T/australien-api)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/e8b63250ee2043f9b07de90531e7fdd7)](https://www.codacy.com/app/Alexandre-T/australien-api?utm_source=github.com&amp;utm_medium=referral&amp;utm_content=Alexandre-T/australien-api&amp;utm_campaign=Badge_Grade)
[![Codacy Badge](https://api.codacy.com/project/badge/Coverage/e8b63250ee2043f9b07de90531e7fdd7)](https://www.codacy.com/app/Alexandre-T/australien-api?utm_source=github.com&utm_medium=referral&utm_content=Alexandre-T/australien-api&utm_campaign=Badge_Coverage)
